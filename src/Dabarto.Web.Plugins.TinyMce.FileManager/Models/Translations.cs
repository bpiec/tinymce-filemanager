﻿using System;
using System.Collections.Generic;

namespace Dabarto.Web.Plugins.TinyMce.FileManager.Models
{
    public static class Translations
    {
        public enum Term
        {
            FileExtensionNotAllowed,
            UploadExceedsMaxFilesize,
            ReturnToFilesList,
            UploadFile,
            NewFolder,
            Filter,
            All,
            Files,
            Images,
            Archives,
            Videos,
            Music,
            Download,
            Erase,
            Open,
            Folder,
            ConfirmationDeleteFolder,
            ConfirmationDeleteFile,
            Preview,
            Select,
            InsertFolderName
        }

        public enum Language
        {
            English,
            Polish
        }

        private static Dictionary<Term, string> _en;
        private static Dictionary<Term, string> _pl;

        static Translations()
        {
            _en = new Dictionary<Term, string>();
            _pl = new Dictionary<Term, string>();

            _en.Add(Term.FileExtensionNotAllowed, "File extension is not allowed");
            _en.Add(Term.UploadExceedsMaxFilesize, "The upload exceeds the max filesize allowed");
            _en.Add(Term.ReturnToFilesList, "Return to files list");
            _en.Add(Term.UploadFile, "Upload a file");
            _en.Add(Term.NewFolder, "New Folder");
            _en.Add(Term.Filter, "Filter");
            _en.Add(Term.All, "All");
            _en.Add(Term.Files, "Files");
            _en.Add(Term.Images, "Images");
            _en.Add(Term.Archives, "Archives");
            _en.Add(Term.Videos, "Videos");
            _en.Add(Term.Music, "Music");
            _en.Add(Term.Download, "Download");
            _en.Add(Term.Erase, "Erase");
            _en.Add(Term.Open, "Open");
            _en.Add(Term.Folder, "folder");
            _en.Add(Term.ConfirmationDeleteFolder, "Are you sure to delete the folder and all the objects in it?");
            _en.Add(Term.ConfirmationDeleteFile, "Are you sure to delete this file?");
            _en.Add(Term.Preview, "Preview");
            _en.Add(Term.Select, "Select");
            _en.Add(Term.InsertFolderName, "Insert folder name:");

            _pl.Add(Term.FileExtensionNotAllowed, "Dany typ pliku jest niedozwolony");
            _pl.Add(Term.UploadExceedsMaxFilesize, "Plik jest za duży");
            _pl.Add(Term.ReturnToFilesList, "Powrót do listy plików");
            _pl.Add(Term.UploadFile, "Wgraj plik");
            _pl.Add(Term.NewFolder, "Nowy folder");
            _pl.Add(Term.Filter, "Filtr");
            _pl.Add(Term.All, "Wszystko");
            _pl.Add(Term.Files, "Pliki");
            _pl.Add(Term.Images, "Obrazki");
            _pl.Add(Term.Archives, "Archiwa");
            _pl.Add(Term.Videos, "Wideo");
            _pl.Add(Term.Music, "Muzyka");
            _pl.Add(Term.Download, "Pobierz");
            _pl.Add(Term.Erase, "Skasuj");
            _pl.Add(Term.Open, "Otwórz");
            _pl.Add(Term.Folder, "folder");
            _pl.Add(Term.ConfirmationDeleteFolder, "Czy na pewno chcesz skasować ten folder i jego zawartość?");
            _pl.Add(Term.ConfirmationDeleteFile, "Czy na pewno chcesz skasować ten plik?");
            _pl.Add(Term.Preview, "Podgląd");
            _pl.Add(Term.Select, "Wybierz");
            _pl.Add(Term.InsertFolderName, "Wpisz nazwę folderu:");
        }

        public static string GetTranslation(Term term, Language language)
        {
            switch (language)
            {
                case Language.English:
                    return _en[term];

                case Language.Polish:
                    return _pl[term];

                default:
                    throw new ArgumentOutOfRangeException("language");
            }
        }
    }
}