﻿namespace Dabarto.Web.Plugins.TinyMce.FileManager.Models
{
    public class FileItem
    {
        public string Name
        {
            get;
            set;
        }

        public int ColNum // list is 6 columns wide, which one is this?
        {
            get;
            set;
        }

        public bool IsFolder
        {
            get;
            set;
        }

        public bool IsFolderUp
        {
            get;
            set;
        }

        public bool IsImage
        {
            get;
            set;
        }

        public bool IsVideo
        {
            get;
            set;
        }

        public bool IsMusic
        {
            get;
            set;
        }

        public bool IsMisc
        {
            get;
            set;
        }

        public string Path
        {
            get;
            set;
        }

        public string Link
        {
            get;
            set;
        }

        public string ClassType
        {
            get;
            set;
        }

        public string DeleteLink
        {
            get;
            set;
        }

        public string PreviewLink
        {
            get;
            set;
        }

        public string ThumbImage
        {
            get;
            set;
        }

        public string DownFormOpen
        {
            get;
            set;
        }
    }
}