﻿using Dabarto.Web.Plugins.TinyMce.FileManager.Properties;
using System;
using System.Web;

namespace Dabarto.Web.Plugins.TinyMce.FileManager.Models
{
    public static class Config
    {
        #region Settings Properties

        /// <summary>
        ///  Max upload filesize in Mb
        /// </summary>
        public static int MaxUploadSizeMb
        {
            get
            {
                return Convert.ToInt32(Settings.Default.MaxUploadSizeMb);
            }
        }

        /// <summary>
        ///  Allowed image file extensions
        /// </summary>
        public static string AllowedImageExtensions
        {
            get
            {
                return Settings.Default.AllowedImageExtensions;
            }
        }

        /// <summary>
        ///  Allowed image file extensions as an array
        /// </summary>
        public static string[] AllowedImageExtensionsArr
        {
            get
            {
                return GetArrayFromString(AllowedImageExtensions);
            }
        }

        /// <summary>
        ///  Allowed document file extensions
        /// </summary>
        public static string AllowedFileExtensions
        {
            get
            {
                return Settings.Default.AllowedFileExtensions;
            }
        }

        /// <summary>
        ///  Allowed document file extensions as an array
        /// </summary>
        public static string[] AllowedFileExtensionsArr
        {
            get
            {
                return GetArrayFromString(AllowedFileExtensions);
            }
        }

        /// <summary>
        ///  Allowed video file extensions
        /// </summary>
        public static string AllowedVideoExtensions
        {
            get
            {
                return Settings.Default.AllowedVideoExtensions;
            }
        }

        /// <summary>
        ///  Allowed video file extensions as an array
        /// </summary>
        public static string[] AllowedVideoExtensionsArr
        {
            get
            {
                return GetArrayFromString(AllowedVideoExtensions);
            }
        }

        /// <summary>
        ///  Allowed music file extensions
        /// </summary>
        public static string AllowedMusicExtensions
        {
            get
            {
                return Settings.Default.AllowedMusicExtensions;
            }
        }

        /// <summary>
        ///  Allowed music file extensions as an array
        /// </summary>
        public static string[] AllowedMusicExtensionsArr
        {
            get
            {
                return GetArrayFromString(AllowedMusicExtensions);
            }
        }

        /// <summary>
        ///  Allowed misc file extensions
        /// </summary>
        public static string AllowedMiscExtensions
        {
            get
            {
                return Settings.Default.AllowedMiscExtensions;
            }
        }

        /// <summary>
        ///  Allowed misc file extensions as an array
        /// </summary>
        public static string[] AllowedMiscExtensionsArr
        {
            get
            {
                return GetArrayFromString(AllowedMiscExtensions);
            }
        }

        /// <summary>
        ///  All allowed file extensions
        /// </summary>
        public static string AllowedAllExtensions
        {
            get
            {
                string strRet = "";

                if (AllowedImageExtensions.Length > 0)
                {
                    strRet = AllowedImageExtensions;
                }
                if (AllowedFileExtensions.Length > 0)
                {
                    if (strRet.Length > 0)
                    {
                        strRet += "," + AllowedFileExtensions;
                    }
                    else
                    {
                        strRet = AllowedFileExtensions;
                    }
                }
                if (AllowedVideoExtensions.Length > 0)
                {
                    if (strRet.Length > 0)
                    {
                        strRet += "," + AllowedVideoExtensions;
                    }
                    else
                    {
                        strRet = AllowedVideoExtensions;
                    }
                }
                if (AllowedMusicExtensions.Length > 0)
                {
                    if (strRet.Length > 0)
                    {
                        strRet += "," + AllowedMusicExtensions;
                    }
                    else
                    {
                        strRet = AllowedMusicExtensions;
                    }
                }
                if (AllowedMiscExtensions.Length > 0)
                {
                    if (strRet.Length > 0)
                    {
                        strRet += "," + AllowedMiscExtensions;
                    }
                    else
                    {
                        strRet = AllowedMiscExtensions;
                    }
                }

                return strRet;
            }
        }

        /// <summary>
        /// Returns document root
        /// </summary>
        public static string DocRoot
        {
            get
            {
                //return HttpContext.Current.Request.ServerVariables["APPL_PHYSICAL_PATH"].TrimEnd('\\');
                return HttpContext.Current.Server.MapPath("/").TrimEnd('\\');
            }
        }

        /// <summary>
        /// Returns the base url of the site
        /// </summary>
        public static string BaseUrl
        {
            get
            {
                return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority.TrimEnd('/');
            }
        }

        /// <summary>
        /// Returns the full upload drive path
        /// </summary>
        public static string UploadPath
        {
            get
            {
                return DocRoot + "\\" + Settings.Default.UploadPath.TrimEnd('\\') + "\\";
            }
        }

        /// <summary>
        /// Returns the full thumb drive path
        /// </summary>
        public static string ThumbPath
        {
            get
            {
                return DocRoot + "\\" + Settings.Default.ThumbPath.TrimEnd('\\') + "\\";
            }
        }

        /// <summary>
        /// Returns the full upload url
        /// </summary>
        public static string UploadUrl
        {
            get
            {
                return BaseUrl + "/" + Settings.Default.UploadPath.Replace('\\', '/');
            }
        }

        /// <summary>
        /// Returns the full thumb url
        /// </summary>
        public static string ThumbUrl
        {
            get
            {
                return BaseUrl + "/" + Settings.Default.ThumbPath.Replace('\\', '/');
            }
        }

        /// <summary>
        /// Returns the setting for allowing upload of file
        /// </summary>
        public static bool AllowUploadFile
        {
            get
            {
                return Convert.ToBoolean(Settings.Default.AllowUploadFile);
            }
        }

        /// <summary>
        /// Returns the setting for allowing delete of file
        /// </summary>
        public static bool AllowDeleteFile
        {
            get
            {
                return Convert.ToBoolean(Settings.Default.AllowDeleteFile);
            }
        }

        /// <summary>
        /// Returns the setting for allowing creation of folder
        /// </summary>
        public static bool AllowCreateFolder
        {
            get
            {
                return Convert.ToBoolean(Settings.Default.AllowCreateFolder);
            }
        }

        /// <summary>
        /// Returns the setting for allowing delete of folder
        /// </summary>
        public static bool AllowDeleteFolder
        {
            get
            {
                return Convert.ToBoolean(Settings.Default.AllowDeleteFolder);
            }
        }

        #endregion Settings Properties

        private static string[] GetArrayFromString(string strInput)
        {
            //remove lead and trail single quotes so we can SPLIT the hell out of it
            var strTemp = strInput.Trim('\'');
            var arrExt = strTemp.Split(new[]
                                       {
                                           "'",
                                           ",",
                                           "'"
                                       }, StringSplitOptions.RemoveEmptyEntries);

            return arrExt;
        } // getArrayFromString
    } // class
}