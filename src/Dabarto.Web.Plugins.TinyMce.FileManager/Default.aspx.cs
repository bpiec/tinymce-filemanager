﻿using Dabarto.Web.Plugins.TinyMce.FileManager.Models;
using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Web.UI;

namespace Dabarto.Web.Plugins.TinyMce.FileManager
{
    public partial class Default : Page
    {
        protected string strType;
        protected string strEditor;
        protected string strCurrPath;
        protected ArrayList arrLinks = new ArrayList();
        protected Translations.Language language;

        private string strApply;
        private string strCmd;
        private string strFolder;
        private string strFile;
        private string strLang;
        private string strCurrLink; // Default.aspx?editor=.... for simplicity

        private int intColNum;
        private string[] arrFolders;
        private string[] arrFiles;
        private FileItem objFItem;
        private bool boolOnlyImage;
        private bool boolOnlyVideo;

        protected void Page_Load(object sender, EventArgs e)
        {
            strCmd = Request.QueryString["cmd"] + "";
            strType = Request.QueryString["type"] + "";
            strFolder = Request.QueryString["folder"] + "";
            strFile = Request.QueryString["file"] + "";
            strLang = Request.QueryString["lang"] + ""; //not used right now, but grab it
            strEditor = Request.QueryString["editor"] + "";
            strCurrPath = Request.QueryString["currpath"] + "";

            language = strLang.ToLower() == "pl" ? Translations.Language.Polish : Translations.Language.English;

            //check inputs
            if (strCurrPath.Length > 0)
            {
                strCurrPath = strCurrPath.TrimEnd('\\') + "\\";
            }

            //set the apply string, based on the passed type
            if (strType == "")
            {
                strType = "0";
            }
            switch (strType)
            {
                case "1":
                    strApply = "apply_img";
                    boolOnlyImage = true;
                    break;

                case "2":
                    strApply = "apply_link";
                    break;

                default:
                    if (Convert.ToInt32(strType) >= 3)
                    {
                        strApply = "apply_video";
                        boolOnlyVideo = true;
                    }
                    else
                    {
                        strApply = "apply";
                    }
                    break;
            }

            //setup current link
            strCurrLink = "Default.aspx?type=" + strType + "&editor=" + strEditor + "&lang=" + strLang;

            switch (strCmd)
            {
                case "debugsettings":
                    Response.Write("<b>AllowCreateFolder:</b> " + Config.AllowCreateFolder + "<br>");
                    Response.Write("<b>AllowDeleteFile:</b> " + Config.AllowDeleteFile + "<br>");
                    Response.Write("<b>AllowDeleteFolder:</b> " + Config.AllowDeleteFolder + "<br>");
                    Response.Write("<b>AllowUploadFile:</b> " + Config.AllowUploadFile + "<br>");
                    Response.Write("<b>MaxUploadSizeMb:</b> " + Config.MaxUploadSizeMb + "<br>");
                    Response.Write("<b>AllowedAllExtensions:</b> " + Config.AllowedAllExtensions + "<br>");
                    Response.Write("<b>AllowedFileExtensions:</b> " + Config.AllowedFileExtensions + "<br>");
                    Response.Write("<b>AllowedImageExtensions:</b> " + Config.AllowedImageExtensions + "<br>");
                    Response.Write("<b>AllowedMiscExtensions:</b> " + Config.AllowedMiscExtensions + "<br>");
                    Response.Write("<b>AllowedMusicExtensions:</b> " + Config.AllowedMusicExtensions + "<br>");
                    Response.Write("<b>AllowedVideoExtensions:</b> " + Config.AllowedVideoExtensions + "<br>");
                    Response.Write("<b>BaseURL:</b> " + Config.BaseUrl + "<br>");
                    Response.Write("<b>DocRoot:</b> " + Config.DocRoot + "<br>");
                    Response.Write("<b>ThumbPath:</b> " + Config.ThumbPath + "<br>");
                    Response.Write("<b>ThumbURL:</b> " + Config.ThumbUrl + "<br>");
                    Response.Write("<b>UploadPath:</b> " + Config.UploadPath + "<br>");
                    Response.Write("<b>UploadURL:</b> " + Config.UploadUrl + "<br>");
                    Response.End();
                    break;

                case "createfolder":
                    try
                    {
                        strFolder = Request.Form["folder"] + "";
                        //forge ahead without checking for existence
                        //catch will save us
                        Directory.CreateDirectory(Config.UploadPath + "\\" + strFolder);
                        Directory.CreateDirectory(Config.ThumbPath + "\\" + strFolder);

                        // end response, since it's an ajax call
                        Response.End();
                    }
                    catch
                    {
                        //TODO: write error
                    }
                    break;

                case "upload":
                    strFolder = Request.Form["folder"] + "";
                    var filUpload = Request.Files["file"];

                    //check file was submitted
                    if ((filUpload != null) && (filUpload.ContentLength > 0))
                    {
                        var strTargetFile = Config.UploadPath + strFolder + filUpload.FileName;
                        var strThumbFile = Config.ThumbPath + strFolder + filUpload.FileName;
                        filUpload.SaveAs(strTargetFile);

                        if (isImageFile(strTargetFile))
                        {
                            createThumbnail(strTargetFile, strThumbFile);
                        }
                    }

                    // end response
                    Response.End();
                    break;

                case "download":
                    var objFile = new FileInfo(Config.UploadPath + "\\" + strFile);
                    Response.ClearHeaders();
                    Response.AddHeader("Pragma", "private");
                    Response.AddHeader("Cache-control", "private, must-revalidate");
                    Response.AddHeader("Content-Type", "application/octet-stream");
                    Response.AddHeader("Content-Length", objFile.Length.ToString());
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(strFile));
                    Response.WriteFile(Config.UploadPath + "\\" + strFile);
                    break;

                case "delfile":
                    try
                    {
                        File.Delete(Config.UploadPath + "\\" + strFile);
                        if (File.Exists(Config.ThumbPath + "\\" + strFile))
                        {
                            File.Delete(Config.ThumbPath + "\\" + strFile);
                        }
                    }
                    catch
                    {
                        //TODO: set error
                    }
                    goto default;

                case "delfolder":
                    try
                    {
                        Directory.Delete(Config.UploadPath + "\\" + strFolder, true);
                        Directory.Delete(Config.ThumbPath + "\\" + strFolder, true);
                    }
                    catch
                    {
                        //TODO: set error
                    }
                    goto default;

                default: //just a regular page load
                    if (strCurrPath != "")
                    {
                        // add "up one" folder
                        objFItem = new FileItem
                        {
                            Name = "..",
                            IsFolder = true,
                            IsFolderUp = true,
                            ColNum = getNextColNum(),
                            Path = Uri.EscapeUriString(getUpOneDir(strCurrPath)),
                            ClassType = "dir",
                            DeleteLink = "<a class=\"btn erase-button top-right disabled\" title=\"" + Translations.GetTranslation(Translations.Term.Erase, language) + "\"><i class=\"icon-trash\"></i></a>",
                            ThumbImage = "img/ico/folder_return.png"
                        };
                        objFItem.Link = "<a title=\"" + Translations.GetTranslation(Translations.Term.Open, language) + "\" href=\"" + strCurrLink + "&currpath=" + objFItem.Path + "\"><img class=\"directory-img\" src=\"" + objFItem.ThumbImage + "\" alt=\"" + Translations.GetTranslation(Translations.Term.Folder, language) + "\" /><h3>..</h3></a>";
                        arrLinks.Add(objFItem);
                    }

                    //load folders
                    arrFolders = Directory.GetDirectories(Config.UploadPath + strCurrPath);
                    foreach (var strF in arrFolders)
                    {
                        objFItem = new FileItem
                        {
                            Name = Path.GetFileName(strF),
                            IsFolder = true,
                            ColNum = getNextColNum(),
                            Path = Uri.EscapeUriString(strCurrPath + Path.GetFileName(strF)),
                            ClassType = "dir"
                        };
                        if (Config.AllowDeleteFolder)
                        {
                            objFItem.DeleteLink = "<a href=\"" + strCurrLink + "&cmd=delfolder&folder=" + objFItem.Path + "&currpath=" + strCurrPath + "\" class=\"btn erase-button top-right\" onclick=\"return confirm('" + Translations.GetTranslation(Translations.Term.ConfirmationDeleteFolder, language) + "');\" title=\"" + Translations.GetTranslation(Translations.Term.Erase, language) + "\"><i class=\"icon-trash\"></i></a>";
                        }
                        else
                        {
                            objFItem.DeleteLink = "<a class=\"btn erase-button top-right disabled\" title=\"" + Translations.GetTranslation(Translations.Term.Erase, language) + "\"><i class=\"icon-trash\"></i></a>";
                        }
                        objFItem.ThumbImage = "img/ico/folder.png";
                        objFItem.Link = "<a title=\"" + Translations.GetTranslation(Translations.Term.Open, language) + "\" href=\"" + strCurrLink + "&currpath=" + objFItem.Path + "\"><img class=\"directory-img\" src=\"" + objFItem.ThumbImage + "\" alt=\"" + Translations.GetTranslation(Translations.Term.Folder, language) + "\" /><h3>" + objFItem.Name + "</h3></a>";
                        arrLinks.Add(objFItem);
                    }

                    // load files
                    arrFiles = Directory.GetFiles(Config.UploadPath + strCurrPath);
                    foreach (var strF in arrFiles)
                    {
                        objFItem = new FileItem
                        {
                            Name = Path.GetFileNameWithoutExtension(strF),
                            IsFolder = false,
                            ColNum = getNextColNum(),
                            Path = Uri.EscapeUriString(strCurrPath + Path.GetFileName(strF)),
                            IsImage = isImageFile(Path.GetFileName(strF)),
                            IsVideo = isVideoFile(Path.GetFileName(strF)),
                            IsMusic = isMusicFile(Path.GetFileName(strF)),
                            IsMisc = isMiscFile(Path.GetFileName(strF))
                        };
                        // get display class type
                        if (objFItem.IsImage)
                        {
                            objFItem.ClassType = "2";
                        }
                        else
                        {
                            if (objFItem.IsMisc)
                            {
                                objFItem.ClassType = "3";
                            }
                            else
                            {
                                if (objFItem.IsMusic)
                                {
                                    objFItem.ClassType = "5";
                                }
                                else
                                {
                                    if (objFItem.IsVideo)
                                    {
                                        objFItem.ClassType = "4";
                                    }
                                    else
                                    {
                                        objFItem.ClassType = "1";
                                    }
                                }
                            }
                        }
                        // get delete link
                        if (Config.AllowDeleteFile)
                        {
                            objFItem.DeleteLink = "<a href=\"" + strCurrLink + "&cmd=delfile&file=" + objFItem.Path + "&currpath=" + strCurrPath + "\" class=\"btn erase-button\" onclick=\"return confirm('" + Translations.GetTranslation(Translations.Term.ConfirmationDeleteFile, language) + "');\" title=\"" + Translations.GetTranslation(Translations.Term.Erase, language) + "\"><i class=\"icon-trash\"></i></a>";
                        }
                        else
                        {
                            objFItem.DeleteLink = "<a class=\"btn erase-button disabled\" title=\"" + Translations.GetTranslation(Translations.Term.Erase, language) + "\"><i class=\"icon-trash\"></i></a>";
                        }
                        // get thumbnail image
                        if (objFItem.IsImage)
                        {
                            objFItem.ThumbImage = Config.ThumbUrl + "/" + objFItem.Path.Replace('\\', '/');
                        }
                        else
                        {
                            if (File.Exists(Directory.GetParent(Request.PhysicalPath).FullName + "\\img\\ico\\" + Path.GetExtension(strF).TrimStart('.').ToUpper() + ".png"))
                            {
                                objFItem.ThumbImage = "img/ico/" + Path.GetExtension(strF).TrimStart('.').ToUpper() + ".png";
                            }
                            else
                            {
                                objFItem.ThumbImage = "img/ico/Default.png";
                            }
                        }
                        objFItem.DownFormOpen = "<form action=\"Default.aspx?cmd=download&file=" + objFItem.Path + "\" method=\"post\" class=\"download-form\">";
                        if (objFItem.IsImage)
                        {
                            objFItem.PreviewLink = "<a class=\"btn preview\" title=\"" + Translations.GetTranslation(Translations.Term.Preview, language) + "\" data-url=\"" + Config.UploadUrl + "/" + objFItem.Path.Replace('\\', '/') + "\" data-toggle=\"lightbox\" href=\"#previewLightbox\"><i class=\"icon-eye-open\"></i></a>";
                        }
                        else
                        {
                            objFItem.PreviewLink = "<a class=\"btn preview disabled\" title=\"" + Translations.GetTranslation(Translations.Term.Preview, language) + "\"><i class=\"icon-eye-open\"></i></a>";
                        }
                        objFItem.Link = "<a href=\"#\" title=\"" + Translations.GetTranslation(Translations.Term.Select, language) + "\" onclick=\"" + strApply + "('" + Config.UploadUrl + "/" + objFItem.Path.Replace('\\', '/') + "'," + strType + ")\";\"><img data-src=\"holder.js/140x100\" alt=\"140x100\" src=\"" + objFItem.ThumbImage + "\" height=\"100\"><h4>" + objFItem.Name + "</h4></a>";

                        // check to see if it's the type of file we are looking at
                        if ((boolOnlyImage && objFItem.IsImage) || (boolOnlyVideo && objFItem.IsVideo) || (!boolOnlyImage && !boolOnlyVideo))
                        {
                            arrLinks.Add(objFItem);
                        }
                    } // foreach

                    break;
            } // switch
        } // page load

        protected string getBreadCrumb()
        {
            string[] arrFolders;
            var strTempPath = "";
            var intCount = 0;

            var strRet = "<li><a href=\"" + strCurrLink + "&currpath=\"><i class=\"icon-home\"></i></a>";
            arrFolders = strCurrPath.Split('\\');

            foreach (var strFolder in arrFolders)
            {
                if (strFolder != "")
                {
                    strTempPath += strFolder + "\\";
                    intCount++;

                    if (intCount == (arrFolders.Length - 1))
                    {
                        strRet += " <span class=\"divider\">/</span></li> <li class=\"active\">" + strFolder + "</li>";
                    }
                    else
                    {
                        strRet += " <span class=\"divider\">/</span></li> <li><a href=\"" + strCurrLink + "&currpath=" + strTempPath + "\">" + strFolder + "</a>";
                    }
                }
            } // foreach

            return strRet;
        } // getBreadCrumb

        private bool isImageFile(string strFilename)
        {
            var intPosition = Array.IndexOf(Config.AllowedImageExtensionsArr, Path.GetExtension(strFilename).TrimStart('.'));
            return (intPosition > -1); // if > -1, then it was found in the list of image file extensions
        } // isImageFile

        private bool isVideoFile(string strFilename)
        {
            var intPosition = Array.IndexOf(Config.AllowedVideoExtensionsArr, Path.GetExtension(strFilename).TrimStart('.'));
            return (intPosition > -1); // if > -1, then it was found in the list of video file extensions
        } // isVideoFile

        private bool isMusicFile(string strFilename)
        {
            var intPosition = Array.IndexOf(Config.AllowedMusicExtensionsArr, Path.GetExtension(strFilename).TrimStart('.'));
            return (intPosition > -1); // if > -1, then it was found in the list of music file extensions
        } // isMusicFile

        private bool isMiscFile(string strFilename)
        {
            var intPosition = Array.IndexOf(Config.AllowedMiscExtensionsArr, Path.GetExtension(strFilename).TrimStart('.'));
            return (intPosition > -1); // if > -1, then it was found in the list of misc file extensions
        } // isMiscFile

        private void createThumbnail(string strFilename, string strThumbFilename)
        {
            var objUnits = GraphicsUnit.Pixel;
            var intHeight = 0;
            var intWidth = 0;

            // open image and get dimensions in pixels
            var objFsImage = Image.FromFile(strFilename);
            var objRect = objFsImage.GetBounds(ref objUnits);

            // what are we going to resize to, to fit inside 156x78
            getProportionalResize(Convert.ToInt32(objRect.Width), Convert.ToInt32(objRect.Height), ref intWidth, ref intHeight);

            // create thumbnail
            var objCallback = new Image.GetThumbnailImageAbort(ThumbnailCallback);
            var objTnImage = objFsImage.GetThumbnailImage(intWidth, intHeight, objCallback, IntPtr.Zero);

            // finish up
            objFsImage.Dispose();
            objTnImage.Save(strThumbFilename);
            objTnImage.Dispose();
        } // createThumbnail

        private void getProportionalResize(int intOldWidth, int intOldHeight, ref int intNewWidth, ref int intNewHeight)
        {
            var intHDiff = 0;
            var intWDiff = 0;
            decimal decProp = 0;
            var intTargH = 78;
            var intTargW = 156;

            if ((intOldHeight <= intTargH) && (intOldWidth <= intTargW))
            {
                // no resize needed
                intNewHeight = intOldHeight;
                intNewWidth = intOldWidth;
                return;
            }

            //get the differences between desired and current height and width
            intHDiff = intOldHeight - intTargH;
            intWDiff = intOldWidth - intTargW;

            //whichever is the bigger difference is the chosen proportion
            if (intHDiff > intWDiff)
            {
                decProp = (decimal)intTargH / (decimal)intOldHeight;
                intNewHeight = intTargH;
                intNewWidth = Convert.ToInt32(Math.Round(intOldWidth * decProp, 0));
            }
            else
            {
                decProp = (decimal)intTargW / (decimal)intOldWidth;
                intNewWidth = intTargW;
                intNewHeight = Convert.ToInt32(Math.Round(intOldHeight * decProp, 0));
            }
        } // getProportionalResize

        private bool ThumbnailCallback()
        {
            return false;
        } // ThumbnailCallback

        protected string getEndOfLine(int intColNum)
        {
            if (intColNum == 6)
            {
                return "</div><div class=\"space10\"></div>";
            }
            else
            {
                return "";
            }
        } // getEndOfLine

        protected string getStartOfLine(int intColNum)
        {
            if (intColNum == 1)
            {
                return "<div class=\"row-fluid\">";
            }
            else
            {
                return "";
            }
        } // getStartOfLine

        private int getNextColNum()
        {
            intColNum++;
            if (intColNum > 6)
            {
                intColNum = 1;
            }
            return intColNum;
        } // getNextColNum

        private string getUpOneDir(string strInput)
        {
            var arrTemp = strInput.TrimEnd('\\').Split('\\');
            arrTemp[arrTemp.Length - 1] = "";
            return String.Join("\\", arrTemp);
        }
    } // class
}