﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Dabarto.Web.Plugins.TinyMce.FileManager.Default" %>

<%@ Import Namespace="Dabarto.Web.Plugins.TinyMce.FileManager.Models" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex,nofollow">
    <title>Tiny File Manager</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-lightbox.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/dropzone.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery.1.9.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-lightbox.min.js"></script>
    <script type="text/javascript" src="js/dropzone.min.js"></script>
    <script>
        var ext_img=new Array(<% Response.Write(Config.AllowedImageExtensions); %>);
        var allowed_ext=new Array(<% Response.Write(Config.AllowedAllExtensions); %>);
        var track = '<% Response.Write(strEditor); %>';
        var curr_dir = '<% Response.Write(strCurrPath.Replace("\\", "\\\\")); %>';

        //dropzone config
        Dropzone.options.myAwesomeDropzone = {
            dictInvalidFileType: "<%= Translations.GetTranslation(Translations.Term.FileExtensionNotAllowed, language) %>",
            dictFileTooBig: "<%= Translations.GetTranslation(Translations.Term.UploadExceedsMaxFilesize, language) %>",
            dictResponseError: "SERVER ERROR",
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: <% Response.Write(Config.MaxUploadSizeMb); %>, // MB
            accept: function(file, done) {
                var extension=file.name.split('.').pop().toLowerCase();
                if ($.inArray(extension, allowed_ext) > -1) {
                    done();
                } else {
                    done("<%= Translations.GetTranslation(Translations.Term.FileExtensionNotAllowed, language) %>");
                }
            }
        };
    </script>
    <script type="text/javascript" src="js/include.js"></script>
</head>
<body>

    <!----- uploader div start ------->
    <div class="uploader">
        <form action="Default.aspx?cmd=upload" id="myAwesomeDropzone" class="dropzone">
            <input type="hidden" name="folder" value="<% Response.Write(strCurrPath); %>" />
            <div class="fallback">
                <input name="file" type="file" multiple />
            </div>
        </form>
        <center><button class="btn btn-large btn-primary close-uploader"><i class="icon-backward icon-white"></i> <%= Translations.GetTranslation(Translations.Term.ReturnToFilesList, language) %></button></center>
        <div class="space10"></div>
        <div class="space10"></div>
    </div>
    <!----- uploader div end ------->

    <div class="container-fluid">

        <!----- header div start ------->
        <div class="filters">
            <% if (Config.AllowUploadFile)
                { %>
            <button class="btn btn-primary upload-btn" style="margin-left: 5px;"><i class="icon-upload icon-white"></i><%= Translations.GetTranslation(Translations.Term.UploadFile, language) %></button>
            <% } %>
            <% if (Config.AllowCreateFolder)
                { %>
            <button class="btn new-folder" style="margin-left: 5px;" data-term="<%= Translations.GetTranslation(Translations.Term.InsertFolderName, language) %>"><i class="icon-folder-open"></i><%= Translations.GetTranslation(Translations.Term.NewFolder, language) %></button>
            <% } %>
            <% if ((Convert.ToInt32(strType) != 1) && (Convert.ToInt32(strType) < 3))
                { // not only image or only video %>
            <div class="pull-right">
                <%= Translations.GetTranslation(Translations.Term.Filter, language) %>: &nbsp;&nbsp;

                <input id="select-type-all" name="radio-sort" type="radio" data-item="ff-item-type-all" class="hide" />
                <label id="ff-item-type-all" for="select-type-all" class="btn btn-info ff-label-type-all"><%= Translations.GetTranslation(Translations.Term.All, language) %></label>
                &nbsp;

                <input id="select-type-1" name="radio-sort" type="radio" data-item="ff-item-type-1" checked="checked" class="hide" />
                <label id="ff-item-type-1" for="select-type-1" class="btn ff-label-type-1"><%= Translations.GetTranslation(Translations.Term.Files, language) %></label>
                &nbsp;

                <input id="select-type-2" name="radio-sort" type="radio" data-item="ff-item-type-2" class="hide" />
                <label id="ff-item-type-2" for="select-type-2" class="btn ff-label-type-2"><%= Translations.GetTranslation(Translations.Term.Images, language) %></label>
                &nbsp;

                <input id="select-type-3" name="radio-sort" type="radio" data-item="ff-item-type-3" class="hide" />
                <label id="ff-item-type-3" for="select-type-3" class="btn ff-label-type-3"><%= Translations.GetTranslation(Translations.Term.Archives, language) %></label>
                &nbsp;

                <input id="select-type-4" name="radio-sort" type="radio" data-item="ff-item-type-4" class="hide" />
                <label id="ff-item-type-4" for="select-type-4" class="btn ff-label-type-4"><%= Translations.GetTranslation(Translations.Term.Videos, language) %></label>
                &nbsp;

                <input id="select-type-5" name="radio-sort" type="radio" data-item="ff-item-type-5" class="hide" />
                <label id="ff-item-type-5" for="select-type-5" class="btn ff-label-type-5"><%= Translations.GetTranslation(Translations.Term.Music, language) %></label>
            </div>
            <% } %>
        </div>
        <!----- header div end ------->

        <!----- breadcrumb div start ------->
        <div class="row-fluid">
            <ul class="breadcrumb">
                <%= getBreadCrumb() %>
            </ul>
        </div>
        <!----- breadcrumb div end ------->

        <div class="row-fluid ff-container">
            <div class="span12 pull-right">
                <ul class="thumbnails ff-items">

                    <%
                        // loop through folder/file list that we have already created
                        foreach (FileItem objF in arrLinks)
                        {
                            //get start of line html, if necessary
                            Response.Write(getStartOfLine(objF.ColNum));

                            // start of item
                            Response.Write("<li class=\"span2 ff-item-type-" + objF.ClassType + "\">");
                            Response.Write("<div class=\"boxes thumbnail\">");

                            if (objF.IsFolder)
                            {
                                // if folder
                                Response.Write(objF.DeleteLink);
                                Response.Write(objF.Link);
                            }
                            else
                            {
                                // if file
                                Response.Write(objF.DownFormOpen);
                                Response.Write("<div class=\"btn-group toolbox\">");
                                Response.Write("<button type=\"submit\" title=\"" + Translations.GetTranslation(Translations.Term.Download, language) + "\" class=\"btn\"><i class=\"icon-download\"></i></button>");
                                Response.Write(objF.PreviewLink);
                                Response.Write(objF.DeleteLink);
                                Response.Write("</div>");
                                Response.Write("</form>");
                                Response.Write(objF.Link);
                            }

                            // end of item
                            Response.Write("</div>");
                            Response.Write("</li>");

                            //get end of line html, if necessary
                            Response.Write(getEndOfLine(objF.ColNum));
                        }
                    %>
                </ul>
            </div>
        </div>
    </div>

    <!----- lightbox div end ------->
    <div id="previewLightbox" class="lightbox hide fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class='lightbox-content'>
            <img id="full-img" src="">
        </div>
    </div>
    <!----- lightbox div end ------->
</body>
</html>